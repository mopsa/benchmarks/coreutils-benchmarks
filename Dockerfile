# Docker image to run Mospa on Coreutils programs
# Build with: docker build -t mopsa-coreutils .
#
###############################################

FROM mopsa/mopsa-build:latest

ENV COREUTILS_VERSION 8.30

# mopsa-build comes with `clang-10` binary and its
# symbolic link `clang` is not present. So add it
# in home directory and update PATH.
##

RUN \
    mkdir bin && \
    ln -s /usr/bin/clang-10 bin/clang

ENV PATH="/home/mopsa/bin:${PATH}"

# copy necessary files
##

COPY support/mopsa.patch /tmp


# download coreutils sources
##

RUN \
    mkdir -p src && \
    wget https://ftp.gnu.org/gnu/coreutils/coreutils-$COREUTILS_VERSION.tar.xz && \
    tar xaf coreutils-$COREUTILS_VERSION.tar.xz -C src && \
    rm coreutils-$COREUTILS_VERSION.tar.xz && \
    patch -p1 < /tmp/mopsa.patch


# compile coreutils
##

RUN \
    cd src/coreutils-$COREUTILS_VERSION && \
    ./configure CFLAGS="-O0" CC=clang && \
    make
